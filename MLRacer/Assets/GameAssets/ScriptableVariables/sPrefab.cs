﻿using UnityEngine;

[CreateAssetMenu(fileName = "sPrefab", menuName = "sVariables/sPrefab", order = 1)]
public class sPrefab : sVariable<GameObject>
{
    // this will only work with prefabs and not instances of gameobjects
    // public GameObject Value;//{ get{  return Value; } set { this.Value = value; } }

    public void SetValue(GameObject _value)
    {
        Value = _value;
    }

    public void SetValue(sPrefab _value)
    {
        Value = _value.Value;
    }
}