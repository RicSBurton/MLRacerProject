﻿using UnityEngine;

[CreateAssetMenu(fileName = "sVector3", menuName = "sVariables/sVector3", order = 1)]
public class sVector3 : sVariable<Vector3>
{
    //  public Vector3 Value;//{ get{  return Value; } set { this.Value = value; } }

    public void SetValue(Vector3 _value)
    {
        Value = _value;
    }

    public void SetValue(sVector3 _value)
    {
        Value = _value.Value;
    }

    public void AddValue(Vector3 _value)
    {
        Value += _value;
    }

    public void AddValue(sVector3 _value)
    {
        Value += _value.Value;
    }
}