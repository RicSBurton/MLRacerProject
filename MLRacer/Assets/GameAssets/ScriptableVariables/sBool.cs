﻿using UnityEngine;

[CreateAssetMenu(fileName = "sBool", menuName = "sVariables/sBool", order = 1)]
public class sBool : sVariable<bool>
{
    //  public bool Value;//{ get{  return Value; } set { this.Value = value; } }

    public void SetValue(bool _value)
    {
        Value = _value;
    }

    public void SetValue(sBool _value)
    {
        Value = _value.Value;
    }

    public void Toggle()
    {
        Value = !Value;
    }
}