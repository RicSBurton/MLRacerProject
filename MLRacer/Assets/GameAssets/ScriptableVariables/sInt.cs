﻿using UnityEngine;

[CreateAssetMenu(fileName = "sint", menuName = "sVariables/sint", order = 1)]
public class sInt : sVariable<int>
{
    //  public int Value;//{ get{  return Value; } set { this.Value = value; } }

    public void SetValue(int _value)
    {
        Value = _value;
    }

    public void SetValue(sInt _value)
    {
        Value = _value.Value;
    }

    public void AddValue(int _value)
    {
        Value += _value;
    }

    public void AddValue(sInt _value)
    {
        Value += _value.Value;
    }
}