﻿using UnityEngine;

public abstract class sVariable<T> : ScriptableObject
{
    public T Value;

    [SerializeField] private T InitialVariable;

    public bool ResetOnDisable = false;

    private void OnEnable()
    {
        InitialVariable = Value;
    }

    private void OnDisable()
    {
        if (ResetOnDisable)
        {
            ResetValue();
        }
    }

    [ContextMenu("Reset To initial Value")]
    public void ResetValue()
    {
        Value = InitialVariable;
    }
}