﻿using UnityEngine;

[CreateAssetMenu(fileName = "sFloat", menuName = "sVariables/sFloat", order = 1)]
public class sFloat : sVariable<float>
{
    // public float Value;//{ get{  return Value; } set { this.Value = value; } }

    public void SetValue(float _value)
    {
        Value = _value;
    }

    public void SetValue(sFloat _value)
    {
        Value = _value.Value;
    }

    public void AddValue(float _value)
    {
        Value += _value;
    }

    public void AddValue(sFloat _value)
    {
        Value += _value.Value;
    }
}