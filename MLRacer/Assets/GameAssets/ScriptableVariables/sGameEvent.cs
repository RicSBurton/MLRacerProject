﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "sGameEvent", menuName = "sVariables/sGameEvent", order = 1)]
public class sGameEvent : ScriptableObject
{
    private List<sGameEventListener> listeners = new List<sGameEventListener>();

    /// <summary>
    ///  // this needs to be raised by a unity event
    /// </summary>
    public void Raise()
    {
        int listenersAmount = listeners.Count;

        for (int i = listenersAmount - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised();
        }
    }

    public void RegisterListener(sGameEventListener listener)
    {
        listeners.Add(listener);
    }

    public void UnregisterListener(sGameEventListener listener)
    {
        listeners.Remove(listener);
    }
}