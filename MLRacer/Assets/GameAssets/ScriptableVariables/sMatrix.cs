﻿using UnityEngine;

[CreateAssetMenu(fileName = "sMatrix", menuName = "sVariables/sMatrix", order = 1)]
public class sMatrix : sVariable<Matrix4x4>
{
    // public Matrix4x4 Value;//{ get{  return Value; } set { this.Value = value; } }

    public void SetValue(Matrix4x4 _value)
    {
        Value = _value;
    }

    public void SetValue(Transform _value)
    {
        Value.SetTRS(_value.position, _value.rotation, _value.localScale);
    }

    public void SetValue(sMatrix _value)
    {
        Value = _value.Value;
    }

    public void AddValue(Matrix4x4 _value)
    {
        //Value.
        //  Value += _value;
    }

    public void AddValue(sMatrix _value)
    {
        //  Value += _value.Value;
    }
}