﻿using UnityEngine;

[CreateAssetMenu(fileName = "sColor", menuName = "sVariables/sColor", order = 1)]
public class sColor : sVariable<Color>
{
    // public Color Value;//{ get{  return Value; } set { this.Value = value; } }

    public void SetValue(Color _value)
    {
        Value = _value;
    }

    public void SetValue(sColor _value)
    {
        Value = _value.Value;
    }
}