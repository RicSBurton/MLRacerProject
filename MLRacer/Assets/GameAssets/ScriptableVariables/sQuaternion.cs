﻿using UnityEngine;

[CreateAssetMenu(fileName = "sQuaternion", menuName = "sVariables/sQuaternion", order = 1)]
public class sQuaternion : sVariable<Quaternion>
{
    // public Quaternion Value;//{ get{  return Value; } set { this.Value = value; } }

    public void SetValue(Quaternion _value)
    {
        Value = _value;
    }

    public void SetValue(sQuaternion _value)
    {
        Value = _value.Value;
    }

    /*
        public void AddValue(Quaternion _value)
        {
            Value *= _value;
        }

        public void AddValue(sQuaternion _value)
        {
            Value *= _value.Value;
        }

        */
}