﻿using UnityEngine;

[CreateAssetMenu(fileName = "sVector2", menuName = "sVariables/sVector2", order = 1)]
public class sVector2 : sVariable<Vector2>
{
    //  public Vector2 Value;//{ get{  return Value; } set { this.Value = value; } }

    public void SetValue(Vector2 _value)
    {
        Value = _value;
    }

    public void SetValue(sVector2 _value)
    {
        Value = _value.Value;
    }

    public void AddValue(Vector2 _value)
    {
        Value += _value;
    }

    public void AddValue(sVector2 _value)
    {
        Value += _value.Value;
    }
}