﻿using UnityEngine;

[CreateAssetMenu(fileName = "sMaterial", menuName = "sVariables/sMaterial", order = 1)]
public class sMaterial : sVariable<Material>
{
    // public Material Value;//{ get{  return Value; } set { this.Value = value; } }

    public void SetValue(Material _value)
    {
        Value = _value;
    }

    public void SetValue(sMaterial _value)
    {
        Value = _value.Value;
    }
}