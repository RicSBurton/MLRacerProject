﻿using UnityEngine;

[CreateAssetMenu(fileName = "sString", menuName = "sVariables/sString", order = 1)]
public class sString : sVariable<string>
{
    //   public string Value;//{ get{  return Value; } set { this.Value = value; } }

    public void SetValue(string _value)
    {
        Value = _value;
    }

    public void SetValue(sString _value)
    {
        Value = _value.Value;
    }
}