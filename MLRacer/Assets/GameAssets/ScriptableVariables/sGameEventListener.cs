﻿using UnityEngine;
using UnityEngine.Events;

public class sGameEventListener : MonoBehaviour
{
    public sGameEvent Event;
    public UnityEvent Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised()
    {
        Response.Invoke();
    }
}