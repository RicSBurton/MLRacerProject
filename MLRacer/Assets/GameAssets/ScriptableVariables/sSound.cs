﻿using UnityEngine;

[CreateAssetMenu(fileName = "sAudioClip", menuName = "sVariables/sAudioClip", order = 1)]
public class sAudioClip : sVariable<AudioClip>
{
    //  public AudioClip Value;//{ get{  return Value; } set { this.Value = value; } }

    public void SetValue(AudioClip _value)
    {
        Value = _value;
    }

    public void SetValue(sAudioClip _value)
    {
        Value = _value.Value;
    }
}