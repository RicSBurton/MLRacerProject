﻿using UnityEngine;

[CreateAssetMenu(fileName = "sAnimationCurve", menuName = "sVariables/sAnimationCurve", order = 1)]
public class sAnimationCurve : sVariable<AnimationCurve>
{
    //public AnimationCurve Value;//{ get{  return Value; } set { this.Value = value; } }

    public void SetValue(AnimationCurve _value)
    {
        Value = _value;
    }

    public void SetValue(sAnimationCurve _value)
    {
        Value = _value.Value;
    }
}