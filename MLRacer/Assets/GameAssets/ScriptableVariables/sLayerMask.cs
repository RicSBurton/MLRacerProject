﻿using UnityEngine;

[CreateAssetMenu(fileName = "sLayerMask", menuName = "sVariables/sLayerMask", order = 1)]
public class sLayerMask : ScriptableObject
{
    public LayerMask Value;
}