﻿using UnityEngine;

public class CheckpointUpdater : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private RacerMovement _racerMovement;

    [SerializeField] private int CounterThreshHold = 100;
    [SerializeField] private int LeftCounter, RightCounter = 0;

    private void Start()
    {
        if (_racerMovement == null)
        {
            _racerMovement = GetComponentInParent<RacerMovement>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Checkpoint currentCheckpoint = other.GetComponent<Checkpoint>();
        if (currentCheckpoint != null)
        {
            _racerMovement.UpdateCheckpoint();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        LeftCounter = 0;
        RightCounter = 0;
    }

    private void OnCollisionStay(Collision collision)
    {
        //Debug.Log(collision.gameObject.name);

        if (string.Compare(collision.gameObject.name, "RightBarrier") == 0)
        {
            RightCounter++;
        }

        if (string.Compare(collision.gameObject.name, "LeftCounter") == 0)
        {
            LeftCounter++;
        }

        if (LeftCounter > CounterThreshHold || RightCounter > CounterThreshHold)
        {
            LeftCounter = RightCounter = 0;
            _racerMovement.Respawn();
        }
    }
}