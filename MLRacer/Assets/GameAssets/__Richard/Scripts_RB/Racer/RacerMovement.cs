﻿using UnityEngine;

public class RacerMovement : MonoBehaviour
{
    [SerializeField] private float _speed;

    [SerializeField] private Rigidbody _myRigidbody;
    [SerializeField] private Transform myTransform;
    [SerializeField] private Transform _centerOfMassTransform;

    [Space]
    [SerializeField] private sFloat _throttleMultiplier;

    [SerializeField] private sVector2 _brakeDrag;
    [SerializeField] private sFloat _yawMultiplier;
    [SerializeField] private sFloat _strafeMultiplier;

    [Space]
    [SerializeField] private PIDData _hoverPID;

    [SerializeField] private sFloat _hoverRayDistance;
    [SerializeField] private sLayerMask _hoverRayLayerMask;
    [SerializeField] private sFloat _hoverHeight;

    [SerializeField]
    private bool _onTrack = false;

    private RaycastHit _hoverRayHit;
    private Ray _hoverRay;

    private Vector3 _lastCheckpointPosition;
    private Quaternion _lastCheckpointRotation;

    //---------------------PID CONTROLLER START--------------

    private float _PIDproportion = 0;//initial force

    private float _PIDintegral = 0; // time taken

    private float _PIDderivative = 0;//damper

    private float _previousError;
    private float P, I, D;

    // Start is called before the first frame update
    private void Start()
    {
        _PIDproportion = _hoverPID.P;
        _PIDintegral = _hoverPID.I;
        _PIDderivative = _hoverPID.D;

        if (myTransform == null)
        {
            myTransform = this.transform;
        }

        if (_myRigidbody == null)
        {
            _myRigidbody = GetComponent<Rigidbody>();
        }

        _myRigidbody.maxAngularVelocity = 100f;

        _myRigidbody.centerOfMass = _centerOfMassTransform.localPosition;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        RacerHover();

        _speed = Vector3.Dot(_myRigidbody.velocity, myTransform.forward);
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="PlayerThrottle"></param>
    /// <param name="PlayerBrake"></param>
    /// <param name="PlayerStrafe"></param>
    /// <param name="PlayerYaw"></param>
    public void RacerInput(float PlayerThrottle, float PlayerBrake, float PlayerStrafe, float PlayerYaw)
    {
        DeltaTime = Time.deltaTime;

        _myRigidbody.AddRelativeTorque(0f, PlayerYaw * _yawMultiplier.Value * DeltaTime, 0f, ForceMode.Acceleration);
        _myRigidbody.AddRelativeForce(PlayerStrafe * _strafeMultiplier.Value * DeltaTime, 0f, 0f, ForceMode.Impulse);
        _myRigidbody.AddRelativeForce(0f, 0f, PlayerThrottle * _throttleMultiplier.Value * DeltaTime, ForceMode.Force);

        _myRigidbody.drag = Mathf.SmoothStep(_brakeDrag.Value.x, _brakeDrag.Value.y, PlayerBrake);
    }

    private void OnDrawGizmos()
    {
        //   Gizmos.DrawRay(_hoverRay.origin, _hoverRay.direction.normalized * _hoverRayDistance.Value, Color.cyan);
        // Gizmos.DrawRay(_hoverRay.origin,_hoverRay.direction*_hoverRayDistance.Value);

        Gizmos.DrawWireSphere(_hoverRayHit.point, 0.5f);
        Gizmos.color = Color.magenta;
        Gizmos.DrawRay(_hoverRayHit.point, _hoverRayHit.normal);
    }

    private float DeltaTime;

    private void RacerHover()
    {
        Vector3 tempVelocity = Quaternion.Inverse(_myRigidbody.rotation) * _myRigidbody.velocity;

        _hoverRay = new Ray(myTransform.position, -myTransform.up);
        DeltaTime = Time.deltaTime;

        //  Debug.DrawRay(_hoverRay.origin, _hoverRay.direction.normalized * _hoverRayDistance.Value, Color.cyan);

        _onTrack = Physics.Raycast(_hoverRay, out _hoverRayHit, _hoverRayDistance.Value, _hoverRayLayerMask.Value);

        if (_onTrack)
        {
            // torque required to rotate to specified rotation
            Vector3 axis = Vector3.Cross(myTransform.up.normalized, _hoverRayHit.normal.normalized); // this gets the axis of rotation
            float torqueAngle = Mathf.Asin(axis.magnitude); // this calculates the angle with the magnitude of the earlier cross product
            Vector3 deltaRotation = axis.normalized * torqueAngle / Time.fixedDeltaTime; // rotation over time

            Vector3 torquerotation = myTransform.rotation *(Quaternion.Inverse(myTransform.rotation) * deltaRotation * 5) * 0.09f;

            Debug.DrawRay(myTransform.position, torquerotation, Color.red);

            // Rotate to track normal using torque
            //  _myRigidbody.AddTorque(myTransform.rotation * (Quaternion.Inverse(myTransform.rotation) * deltaRotation*5) * 0.09f,ForceMode.Impulse);

            _myRigidbody.AddTorque(torquerotation, ForceMode.Impulse);

            _myRigidbody.velocity = _myRigidbody.rotation * tempVelocity;
            // hover force using PID controller
            _myRigidbody.AddRelativeForce(0, PIDoutput(_hoverHeight.Value - _hoverRayHit.distance, DeltaTime), 0, ForceMode.Force);
        }
        else
        {
            Respawn();
        }
    }

    public void UpdateCheckpoint()
    {
        _lastCheckpointPosition = _myRigidbody.position;
        _lastCheckpointRotation = Quaternion.LookRotation(_myRigidbody.velocity, myTransform.up);
    }

    public void Respawn()
    {
        _myRigidbody.velocity = Vector3.zero;

        myTransform.SetPositionAndRotation(_lastCheckpointPosition, _lastCheckpointRotation);
    }

    private void OnCollisionEnter(Collision collision)
    {
        _myRigidbody.AddForce(0, -Vector3.Dot(collision.impulse, myTransform.up), 0f, ForceMode.Impulse);

        //  _myRigidbody.velocity = Vector3.Scale(_myRigidbody.velocity, new Vector3(0.5f, 0.5f, 0.5f));
    }

    private float PIDoutput(float currentError, float DeltaTime)
    {
        P = currentError;
        I += P * DeltaTime;
        D = (P - _previousError) / DeltaTime;
        _previousError = currentError;
        // return (P * _PIDproportion + I * _PIDintegral + D * PIDderivative);

        return (P * _hoverPID.P + I * _hoverPID.I + D * _hoverPID.D);
    }

    //---------------------PID CONTROLLER END--------------
}