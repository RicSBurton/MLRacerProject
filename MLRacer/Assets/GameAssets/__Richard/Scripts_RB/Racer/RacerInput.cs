﻿using UnityEngine;

[RequireComponent(typeof(RacerMovement))]
public class RacerInput : MonoBehaviour
{
    [SerializeField]
    private VirtualInput _virtualInput;

    [SerializeField] private sBool isNetworkControlled;
    [SerializeField] private sFloat networkYaw;
    [SerializeField] private sFloat networkThrottle;

    [SerializeField] private RacerMovement _controlledRacerMovement;

    // Start is called before the first frame update
    private void Start()
    {
        _controlledRacerMovement = GetComponent<RacerMovement>();
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (isNetworkControlled.Value)
        {
            _controlledRacerMovement.RacerInput(
                networkThrottle.Value,
                _virtualInput.LeftTrigger,
                _virtualInput.RightStick.x,
                networkYaw.Value);
        }
        _controlledRacerMovement.RacerInput(
            _virtualInput.RightTrigger,
            _virtualInput.LeftTrigger,
            _virtualInput.RightStick.x,
            _virtualInput.LeftStick.x);
    }
}