﻿using UnityEngine;

public class RigidbodyFollower : MonoBehaviour
{
    private Transform _myTransform;
    private Rigidbody _myRigidbody;

    [SerializeField] private Transform _trackedTransform;
    [SerializeField] private float RotationTrackSpeed = 0.999f, PositionTrackSpeed = 0.999f;

    private void Start()
    {
        if (_myTransform == null)
        {
            _myTransform = this.transform;
        }

        if (_myRigidbody == null)
        {
            _myRigidbody = GetComponent<Rigidbody>();
        }
    }

    // Update is called once per frame
    private void Update()
    {
        _myTransform.SetPositionAndRotation
        (
            Vector3.Lerp(_myTransform.position, _trackedTransform.position, PositionTrackSpeed),
             Quaternion.Lerp(_myTransform.rotation, _trackedTransform.rotation, RotationTrackSpeed)
         );
    }
}