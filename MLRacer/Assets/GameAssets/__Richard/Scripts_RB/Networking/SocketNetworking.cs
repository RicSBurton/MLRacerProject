﻿using SocketIO;
using System;
using UnityEngine;

public class SocketNetworking : MonoBehaviour
{
    [SerializeField]
    private SocketIOComponent socket;

    [SerializeField] private sBool isSendingDataToNetwork;

    [SerializeField] private sBool isNetworkControlled;
    [SerializeField] private sFloat networkYaw;
    [SerializeField] private sFloat networkYawMultipier;

    [SerializeField] private RenderTexture targetTexture;

    [SerializeField] private float steering;
    [SerializeField] private string yawstring;

    // Start is called before the first frame update
    private void Start()
    {
        if (socket == null)
        {
            socket = GameObject.Find("SocketIO").GetComponent<SocketIOComponent>();
        }

        socket.On("steer", NetworkYawEvent);
    }

    private void NetworkYawEvent(SocketIOEvent e)
    {
        yawstring = e.data.GetField("yaw").ToString();
        //Debug.Log(yawstring);
        steering = float.Parse(yawstring.Trim('"'));
        //Debug.Log(steering);
        networkYaw.Value = steering * networkYawMultipier.Value;
        isNetworkControlled.Value = true;
    }

    private void sendImage()
    {
        JSONObject jsondata = new JSONObject();

        string imagedata = Convert.ToBase64String(GetFrame());
        //Debug.Log(imagedata);
        //Debug.Log("sent Image");

        jsondata.Add(imagedata);

        socket.Emit("Image", jsondata);

        //Debug.Log("sending");
    }

    public byte[] GetFrame()
    {
        RenderTexture.active = targetTexture;
        Texture2D texture2D = new Texture2D(targetTexture.width, targetTexture.height, TextureFormat.RGB24, false);
        texture2D.ReadPixels(new Rect(0, 0, targetTexture.width, targetTexture.height), 0, 0);
        texture2D.Apply();
        byte[] image = texture2D.EncodeToPNG();
        UnityEngine.Object.DestroyImmediate(texture2D); // Required to prevent leaking the texture
        return image;
    }

    //void hi(SocketIOEvent e)
    //{
    //    if (e != null)
    //    {
    //        //data = e.data.ToString();
    //        string yawstring = e.data.GetField("yaw").ToString();
    //        float yaw = float.Parse(yawstring);
    //       // Debug.Log(yaw );
    //    }
    //}

    public void TestClose(SocketIOEvent e)
    {
        Debug.Log("[SocketIO] Close received: " + e.name + " " + e.data);
        isNetworkControlled.Value = false;
    }

    private void Update()
    {
        if (isSendingDataToNetwork.Value == true)
        {
            sendImage();
        }
        else
        {
            isNetworkControlled.Value = false;
        }
    }
}