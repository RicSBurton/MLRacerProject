﻿using UnityEngine;

public class TrackDataPopulator : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private TrackPieceData _trackData;

    private Vector3 positionOffset;
    private Quaternion myRotation;

    private Vector3 testEndpoint;

    private Vector3 startPoint;
    private Vector3 EndPoint;

    private void Start()
    {
        //PopulateTrackPieceData();
    }

    [ContextMenu("PopulateTrackPieceData")]
    public void PopulateTrackPieceData()
    {
        GameObject[] endgameobjects = GameObject.FindGameObjectsWithTag("EndPoint");

        foreach (GameObject endgameobject in endgameobjects)
        {
            if (endgameobject.activeInHierarchy == true && endgameobject.transform.IsChildOf(transform))
            {
                EndPoint = endgameobject.GetComponent<Renderer>().bounds.center;
                _trackData.SetTrackPieceData(EndPoint);
                testEndpoint = EndPoint;
                _trackData._name = transform.name;
            }
        }
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.cyan;
    //    Gizmos.DrawWireSphere(_trackData.EndPointPosition,1.0f);
    //    /*
    //            Gizmos.DrawLine(transform.position, transform.position + _trackData.DirectionQuaternion*transform.forward);
    //            Gizmos.color = Color.green;
    //            Gizmos.DrawLine(transform.position, transform.position + _trackData.DirectionQuaternion * transform.up);
    //            Gizmos.color = Color.red;
    //            Gizmos.DrawLine(transform.position, transform.position + _trackData.DirectionQuaternion * transform.right);

    //        */
    //    Gizmos.color = Color.blue;
    //     positionOffset = transform.position + _trackData.EndPointPosition;
    //     myRotation = transform.rotation;

    //    Gizmos.DrawLine(myRotation*positionOffset, positionOffset + (myRotation* _trackData.DirectionQuaternion * transform.forward).normalized);
    //    Gizmos.color = Color.green;
    //    Gizmos.DrawLine(myRotation*positionOffset, positionOffset + (myRotation* _trackData.DirectionQuaternion * transform.up).normalized);
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawLine(myRotation*positionOffset, positionOffset + (myRotation* _trackData.DirectionQuaternion * transform.right).normalized);

    //}
}