﻿using System.Collections;
using UnityEngine;

public class TrackBuilder : MonoBehaviour
{
    [SerializeField]
    private sInt TrackLength;

    [SerializeField] private Transform _startTransform;
    [SerializeField] private TrackPieceData _startPieceData, _endPieceData;
    [SerializeField] private sFloat _angleThreshold;
    [SerializeField] private sInt _trackSeed;

    [SerializeField]
    private TrackPieceData[] TrackPieces;

    // Start is called before the first frame update
    private void Start()
    {
        Random.InitState(_trackSeed.Value);
        StartCoroutine(BuildTrack());
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private IEnumerator BuildTrack()
    {
        int amountOfPieces = TrackPieces.Length;
        Vector3 spawnpoint = _startTransform.position;
        Quaternion spawnRotation = _startTransform.rotation;
        Vector3 startDirection = _startTransform.forward;
        int randomNumber;

        GameObject startTrackPiece = Instantiate(_startPieceData.TrackPiecePrefab,
            spawnpoint, spawnRotation);

        UpdateSpawnPoint(ref spawnpoint, ref spawnRotation, _startPieceData);

        for (int i = 1; i < TrackLength.Value; i++)
        {
            int j = 0;

            do
            {
                randomNumber = Random.Range(0, amountOfPieces - 1);
                j++;

                if (j > 100)
                {
                    Debug.Log("too many loops");
                    break;
                }
            } while (Vector3.Dot(spawnRotation * TrackPieces[randomNumber].DirectionQuaternion * startDirection, startDirection) < _angleThreshold.Value);

            //Debug.Log("track piece: "+ 1 + " "+Vector3.Dot(spawnRotation * TrackPieces[randomNumber].DirectionQuaternion * startDirection, startDirection));

            GameObject newTrackPiece = Instantiate(TrackPieces[randomNumber].TrackPiecePrefab,
                spawnpoint, spawnRotation);
            UpdateSpawnPoint(ref spawnpoint, ref spawnRotation, TrackPieces[randomNumber]);

            //  Debug.Log("hi");
            yield return null;
        }

        GameObject endTrackPiece = Instantiate(_endPieceData.TrackPiecePrefab,
            spawnpoint, spawnRotation);
        UpdateSpawnPoint(ref spawnpoint, ref spawnRotation, _endPieceData);
    }

    private void UpdateSpawnPoint(ref Vector3 spawnPoint, ref Quaternion spawnRotation, TrackPieceData trackPiece)
    {
        spawnPoint += spawnRotation * trackPiece.EndPointPosition;
        spawnRotation *= trackPiece.DirectionQuaternion;// * spawnRotation;
    }
}