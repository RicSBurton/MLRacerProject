﻿using UnityEngine;

public class LocationGizmo : MonoBehaviour
{
    [SerializeField] private Transform startPoint;
    [SerializeField] private Vector3 endPoint;
    [SerializeField] private Vector3 localendPoint;

    // Start is called before the first frame update
    private void Start()
    {
    }

    [ContextMenu("hi")]
    private void hi()
    {
        Debug.Log("hi");
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, .05f);

        Gizmos.DrawWireSphere(endPoint = transform.GetComponent<Renderer>().bounds.center, .01f);

        localendPoint = endPoint - startPoint.position;
    }
}