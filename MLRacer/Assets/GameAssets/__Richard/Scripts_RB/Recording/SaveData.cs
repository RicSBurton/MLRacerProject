﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;

public class SaveData : MonoBehaviour
{
    [ContextMenu("CreateFolder")]
    private void CreateFolder()
    {
        DirectoryInfo folder =
            Directory.CreateDirectory(Application.streamingAssetsPath);

        string date = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
        string rootfolder = Application.streamingAssetsPath + "/record_" + date;

        string pictureFolder = rootfolder + "/Pictures";
        Debug.Log(date);
        try
        {
            if (!Directory.Exists(rootfolder))
            {
                Directory.CreateDirectory(rootfolder);
                Directory.CreateDirectory(pictureFolder);
                Debug.Log("Created: " + rootfolder);
            }
        }
        catch (IOException ex)
        {
            Debug.LogError("writeFailed: " + ex);
        }
    }

    /// <summary>
    /// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// </summary>
    private const string FLOAT_FORMAT = "0.0000";

    [Serializable]
    public struct ImageData
    {
        public float RacerYaw;
        public float RacerStrafe;
        public byte[] Image;
    }

    private Thread encoderThread;
    private Queue<ImageData> imagesQueue;
    private StreamWriter dataWriter;

    private bool isDrone;
    private string docsLocation;
    private string folderLocation;
    private string fileName;
    private string imagesLocation;

    private bool isCapturingImagesRunning;
    private int count = 0;

    //Start the thread to dequeue the capture data and save them in a folder.
    public void StartRecording()
    {
        imagesQueue = new Queue<ImageData>();
        isCapturingImagesRunning = true;
        encoderThread = new Thread(SaveImagesThread);
        encoderThread.Priority = System.Threading.ThreadPriority.BelowNormal;
        encoderThread.Start();
        docsLocation = Application.streamingAssetsPath + "/";
        folderLocation = docsLocation + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
        if (!Directory.Exists(folderLocation))
        {
            Directory.CreateDirectory(folderLocation);
        }

        imagesLocation = "/Pictures";
        imagesLocation = folderLocation + imagesLocation;
        if (!Directory.Exists(imagesLocation))
        {
            Directory.CreateDirectory(imagesLocation);
        }

        fileName = "/RecordedData.csv";
        fileName = folderLocation + fileName;
        dataWriter = new StreamWriter(File.Open(fileName, FileMode.OpenOrCreate, FileAccess.Write));
        string heading;
        // car

        //  heading = "Timestamp\tSpeed (kmph)\tThrottle\tSteering\tBrake\tGear\tImageName";
        heading = "Timestamp,Yaw,strafe,ImageName";

        dataWriter.WriteLine(heading);
    }

    public void StopRecording()
    {
        isCapturingImagesRunning = false;
        if (dataWriter != null)
        {
            dataWriter.Close();
            dataWriter = null;
        }
    }

    public void AddImageDataToQueue(ImageData data)
    {
        if (imagesQueue == null)
        {
            return;
        }

        imagesQueue.Enqueue(data);
    }

    private void SaveImagesThread()
    {
        //  Debug.Log("saving images");
        string imageName;
        while (true)
        {
            if (!isCapturingImagesRunning && imagesQueue.Count <= 0)
            {
                break;
            }
            else if (imagesQueue.Count <= 0)
            {
                Thread.Sleep(500);
                continue;
            }

            long timeStamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(); ;
            ImageData data = imagesQueue.Dequeue();

            imageName = string.Format("img_{0}_{1}.png", count++, timeStamp);

            // File.WriteAllBytes(imageName, data.Image);

            // heading = "Timestamp\tYaw\tstrafe\tImageName";
            //dataWriter.WriteLine(string.Format("{0}\t{1}\t{2}\t{3}",
            //    timeStamp,
            //    data.RacerYaw.ToString(FLOAT_FORMAT),
            //    data.RacerStrafe.ToString(FLOAT_FORMAT),
            //    imageName));

            dataWriter.Write(timeStamp + ",");
            dataWriter.Write(data.RacerYaw.ToString(FLOAT_FORMAT) + ",");
            dataWriter.Write(data.RacerStrafe.ToString(FLOAT_FORMAT) + ",");
            dataWriter.Write(imageName + "\n");

            // Debug.Log(imageName);

            File.WriteAllBytes(imagesLocation + "/" + imageName, data.Image);
            //car
            //dataWriter.WriteLine(string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}",
            //    timeStamp, data.carData.speed, data.carData.throttle.ToString(FLOAT_FORMAT),
            //    data.carData.steering.ToString(FLOAT_FORMAT), data.carData.brake.ToString(FLOAT_FORMAT),
            //    data.carData.gear, imageName));
        }

        if (imagesQueue != null)
        {
            Debug.Log(imagesQueue.Count);
        }

        imagesQueue = null;
        // Debug.Log("saved images");
    }
}

/*

public class DataRecorder
{
    private const string FLOAT_FORMAT = "0.0000";

    public struct ImageData
    {
     //   public CarStructs.CarData carData;
      //  public AirSimPose pose;

        public byte[] image;
    }

    private Thread encoderThread;
    private Queue<ImageData> imagesQueue;
    private StreamWriter dataWriter;

    private bool isDrone;
    private readonly string docsLocation;
    private string folderLocation;
    private string fileName;
    private string imagesLocation;

    private bool isCapturingImagesRunning;
    private int count = 0;

    public DataRecorder()
    {
      //  isDrone = true;
        count = 0;
        docsLocation = Application.streamingAssetsPath;
    }

    //Start the thread to dequeue the capture data and save them in a folder.
    public void StartRecording()
    {
        imagesQueue = new Queue<ImageData>();
        isCapturingImagesRunning = true;
        encoderThread = new Thread(SaveImagesThread);
        encoderThread.Priority = System.Threading.ThreadPriority.BelowNormal;
        encoderThread.Start();

        folderLocation = docsLocation + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
        if (!Directory.Exists(folderLocation))
        {
            Directory.CreateDirectory(folderLocation);
        }

        imagesLocation = "/Pictures";
        imagesLocation = folderLocation + imagesLocation;
        if (!Directory.Exists(imagesLocation))
        {
            Directory.CreateDirectory(imagesLocation);
        }

        fileName =  "/RecordedData.txt" ;
        fileName = folderLocation + fileName;
        dataWriter = new StreamWriter(File.Open(fileName, FileMode.OpenOrCreate, FileAccess.Write));
        string heading;
            // car

            heading = "Timestamp\tSpeed (kmph)\tThrottle\tSteering\tBrake\tGear\tImageName";

        dataWriter.WriteLine(heading);
    }

    public void StopRecording()
    {
        isCapturingImagesRunning = false;
        if (dataWriter != null)
        {
            dataWriter.Close();
            dataWriter = null;
        }
    }

    public void AddImageDataToQueue(ImageData data)
    {
        if (imagesQueue == null)
        {
            return;
        }
        imagesQueue.Enqueue(data);
    }

    private void SaveImagesThread()
    {
        string imageName;
        while (true)
        {
            if (!isCapturingImagesRunning && imagesQueue.Count <= 0)
            {
                break;
            }
            else if (imagesQueue.Count <= 0)
            {
                Thread.Sleep(500);
                continue;
            }

            long timeStamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(); ;
            ImageData data = imagesQueue.Dequeue();

            imageName = string.Format("img_{0}_{1}.png", count++, timeStamp);

                //car
                //dataWriter.WriteLine(string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}",
                //    timeStamp, data.carData.speed, data.carData.throttle.ToString(FLOAT_FORMAT),
                //    data.carData.steering.ToString(FLOAT_FORMAT), data.carData.brake.ToString(FLOAT_FORMAT),
                //    data.carData.gear, imageName));

            File.WriteAllBytes(imageName, data.image);
        }
        imagesQueue = null;
    }
}

    */