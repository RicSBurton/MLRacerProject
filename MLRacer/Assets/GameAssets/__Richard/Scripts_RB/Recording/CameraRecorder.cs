﻿using System.Collections;
using UnityEngine;

public class CameraRecorder : MonoBehaviour
{
    [SerializeField] private SaveData _dataSaver;
    [SerializeField] private RenderTexture recordingRenderTexture;
    [SerializeField] private Texture2D screenshot;
    [SerializeField] private Rect captureRect;
    [SerializeField] private sBool toggleRecordSBool;
    [SerializeField] private sFloat recordTimeInterval;

    [SerializeField] private VirtualInput vController;

    private bool recording = false;

    private void Awake()
    {
        screenshot = new Texture2D(recordingRenderTexture.width, recordingRenderTexture.height);
    }

    // Start is called before the first frame update

    // Update is called once per frame
    private void Update()
    {
        if (toggleRecordSBool.Value == true)
        {
            if (recording == false)
            {
                Debug.Log("recording");
                recording = true;
                StartCoroutine(RecordFrames());

                _dataSaver.StartRecording();
            }
        }
        else
        {
            if (recording == true)
            {
                recording = false;
                StopCoroutine(RecordFrames());
                _dataSaver.StopRecording();
            }
        }
    }

    private IEnumerator RecordFrames()
    {
        while (toggleRecordSBool.Value)
        {
            yield return new WaitForEndOfFrame();

            RenderTexture.active = recordingRenderTexture;

            screenshot.ReadPixels((new Rect(0f, 0f, recordingRenderTexture.width, recordingRenderTexture.height)), 0, 0);
            screenshot.Apply();
            byte[] bytes = screenshot.EncodeToPNG();
            RenderTexture.active = null;
            SaveData.ImageData data;

            data.RacerStrafe = vController.RightStick.x;
            data.RacerYaw = vController.LeftStick.x;
            data.Image = bytes;
            // Debug.Log("added image");
            _dataSaver.AddImageDataToQueue(data);
            yield return new WaitForSecondsRealtime(recordTimeInterval.Value);
        }
    }

    private void OnApplicationQuit()
    {
        if (recording)
        {
            _dataSaver.StopRecording();
        }
    }

    private void OnDisable()
    {
        // StopCoroutine(RecordFrames());
        _dataSaver.StopRecording();
    }
}