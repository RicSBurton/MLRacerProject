﻿using UnityEngine;

namespace Assets.GameAssets.__Richard.Scripts_RB.Depericiated
{
    public class TrackCreator : MonoBehaviour
    {
        [System.Serializable]
        public struct tracktype
        {
            [SerializeField] public GameObject part;
            [SerializeField] public Vector3 angle;// stores the angle of rotation in each axis
        }

        [SerializeField] private GameObject StartingPoint;

        public int trackLength;
        public int trackSeed;

        //[SerializeField]	  GameObject[] trackPart;

        //	[SerializeField]	  GameObject[] trackLeft;
        //	[SerializeField]	  GameObject[] trackRight;

        //	[SerializeField]	  GameObject[] trackUp;
        //	[SerializeField]	  GameObject[] trackDown;

        //	[SerializeField]	  GameObject[] trackTwist;

        //	[SerializeField]	  GameObject[] trackSpecial;

        public GameObject[] Track;
        public int maxangle;

        public tracktype[] TrackPartPrefab;
        private bool built = false;
        private bool buildingtrack = false;

        private int Currentpart = 1;

        private Vector3 trackDirection;

        public GameObject startLine;
        public GameObject finishLine;

        //-----------------------------------------------------------------------------------

        private void OnChangeSeed(int tseed)
        {
            trackSeed = tseed;
        }

        private void OnChangeLength(int tLength)
        {
            trackLength = tLength;
        }

        private void CleanTrack()
        {
            foreach (Transform part in StartingPoint.transform)
            {
                Destroy(part.gameObject);
            }
        }

        private void Reset()
        {
            built = false;
            buildingtrack = false;

            Currentpart = 1;
        }

        private void CreateParts()
        {
            Transform endPoint;
            Transform startPoint;

            trackDirection = Vector3.zero;
            int randomTrackNum;

            if (Currentpart < trackLength - 1)
            {
                endPoint = Track[Currentpart - 1].transform.Find("EndPoint").transform;

                do
                {// when creating new part check parts tracks direction if direction is to larger try new track part
                    randomTrackNum = Random.Range(0, TrackPartPrefab.Length);//creates a random number
                } while
                (trackDirection.x + TrackPartPrefab[randomTrackNum].angle.x > maxangle || trackDirection.x + TrackPartPrefab[randomTrackNum].angle.x < -maxangle ||
                 trackDirection.y + TrackPartPrefab[randomTrackNum].angle.y > maxangle || trackDirection.y + TrackPartPrefab[randomTrackNum].angle.y < -maxangle ||
                 trackDirection.z + TrackPartPrefab[randomTrackNum].angle.z > maxangle || trackDirection.z + TrackPartPrefab[randomTrackNum].angle.z < -maxangle);
                //end of do while loop conditions

                trackDirection += TrackPartPrefab[randomTrackNum].angle;// sets new track direction
                startPoint = TrackPartPrefab[randomTrackNum].part.transform.Find("StartPoint").transform;// sets the start point of the current track piece

                //Instantiates track part in the end childs position of the previous track object
                Track[Currentpart] = Instantiate(TrackPartPrefab[randomTrackNum].part, endPoint.position, endPoint.rotation, StartingPoint.transform) as GameObject;
                Track[Currentpart].transform.Translate(-startPoint.position);

                try
                {
                    Track[Currentpart].GetComponentInChildren<TextMesh>().text = Currentpart.ToString();
                }
                catch (System.Exception ex)
                {
                }

                Currentpart++;
            }
            else if (Currentpart == trackLength - 1)
            {
                built = true;
                buildingtrack = false;
                BuildTrack();
            }
        }

        public void BuildTrack()
        {
            Debug.Log("Building Track");
            //	Bounds b;

            trackDirection = Vector3.zero;
            int randomTrackNum;

            Transform endPoint;
            Transform startPoint;

            if (buildingtrack == false && built == false)
            {
                CleanTrack();
                Random.InitState(trackSeed - 1);// resets the random number
                Random.InitState(trackSeed);

                Debug.Log("seed " + trackSeed);
                //	TrackPartPrefab = new tracktype[trackLength];
                Track = new GameObject[trackLength];

                Track[0] = Instantiate(startLine, StartingPoint.transform) as GameObject;

                //NetworkServer.Spawn (Track [0]);

                Debug.Log(Track[1 - 1].transform.Find("End").transform.position.ToString());

                //	Racer.transform.position = StartingPoint.transform.position;
                //	Racer.transform.rotation = StartingPoint.transform.rotation;
                //	Racer.transform.Translate (0, 5, 0);
                //	Racer.transform.Translate (0, 5, 0);
                //	Racer.transform.Rotate (0, 180, 0);

                built = false;
                buildingtrack = true;
            }

            // this is where the track is built(where the for loop was)

            if (buildingtrack == false && built == true)
            {
                // last part of track
                endPoint = Track[trackLength - 2].transform.Find("End").transform;
                startPoint = finishLine.transform.Find("Start").transform;

                Track[trackLength - 1] = Instantiate(finishLine, StartingPoint.transform) as GameObject;
                Track[trackLength - 1].transform.position = endPoint.position + startPoint.position;
                Track[trackLength - 1].transform.rotation = endPoint.rotation;
            }
        }

        // Use this for initialization
        private void Start()
        {
        }

        // Update is called once per frame
        private void Update()
        {
            //	if (Input.GetKeyDown ("space"))
            //	{
            //		BuildTrack ();
            //	}

            if (built == false && buildingtrack == true)
            {
                CreateParts();
            }
        }
    }
}