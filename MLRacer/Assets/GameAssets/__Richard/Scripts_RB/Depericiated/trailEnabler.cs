﻿using UnityEngine;

public class trailEnabler : MonoBehaviour
{
    public TrailRenderer trail1;
    public TrailRenderer trail2;
    public GameObject Racer;
    private Rigidbody Racerbody;
    public float trailSpeed;

    private float moving = 0;
    private float trailwidth;
    private float trailInitialDuration;
    private float trailDuration;

    // Use this for initialization
    private void Start()
    {
        Racerbody = Racer.GetComponent<Rigidbody>();
        trailwidth = 0;
        trail1.endWidth = 0;
        trail2.endWidth = 0;
        trailInitialDuration = 20;
    }

    // Update is called once per frame
    private void Update()
    {
        //gets the local z velocity
        if ((Racer.transform.InverseTransformDirection(Racerbody.velocity)).z > trailSpeed)
        {
            moving = 1;
        }
        else
        {
            moving = 0;
        }

        trailwidth = Mathf.Lerp(trailwidth, moving, 0.01f);

        trailDuration = Mathf.Lerp(trailDuration, moving, 0.05f);

        if (trail1.startWidth != trailwidth)
        {
            trail1.startWidth = trailwidth;
            trail2.startWidth = trailwidth;
            //	trail1.endWidth = 0;
            //	trail2.endWidth = 0;
            trail1.time = trailDuration * 20;
            trail2.time = trailDuration * 20;
        }
    }
}