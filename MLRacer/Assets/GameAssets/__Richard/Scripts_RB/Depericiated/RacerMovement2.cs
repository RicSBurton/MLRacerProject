﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.GameAssets.__Richard.Scripts_RB.Depericiated
{
    public class RacerMovement2 : MonoBehaviour
    {
        // [SerializeField]
        // RaceManager manager;// set in inspector

        [SerializeField]
        private float speed = 5;

        [SerializeField]
        private float Break = 5;

        [SerializeField]
        private float strafeSpeed = 5;

        [SerializeField]
        private float rotationSpeed = 1;

        [SerializeField]
        private float Hover_Force;

        [SerializeField]
        private float Hover_Ray;// set in inspector

        public Text countdown;

        public bool winState = false;
        private bool onTrack = false;

        private bool ResultsShown = false;
        private bool CanRace = false;
        private bool timeSet = true;

        [SerializeField]
        private float Hover_Height;// set in inspector

        [SerializeField]
        private float strafeDrag = 20;

        public bool RaceFinished = false;

        public double raceTime;

        public double startTime;

        private bool localgo;

        private bool colliding = false;
        private Vector3 safeposition; //=new Vector3 (0,5,0);
        private Quaternion safeRotation;
        private Vector3 lVelocity;
        private Rigidbody rb;

        //---------------------PID CONTROLLER START--------------
        [SerializeField]
        private float PIDproportion = 1;//initial force

        [SerializeField]
        private float PIDintegral = 0; // time taken

        [SerializeField]
        private float PIDderivative = 0.1f;//damper

        private float previousError;
        private float P, I, D;

        private float PIDoutput(float currentError, float DeltaTime)
        {
            P = currentError;
            I += P * DeltaTime;
            D = (P - previousError) / DeltaTime;
            previousError = currentError;
            return (P * PIDproportion + I * PIDintegral + D * PIDderivative);
        }

        //---------------------PID CONTROLLER END--------------

        //---------------------PID angle CONTROLLER START--------------
        public float aPIDproportion = 1;//initial force

        public float aPIDintegral = 0;
        public float aPIDderivative = 0.1f;//damper
        private float apreviousError;
        private float aP, aI, aD;

        private float aPIDoutput(float currentError, float DeltaTime)
        {
            aP = currentError;
            aI += aP * DeltaTime;
            aD = (aP - apreviousError) / DeltaTime;
            apreviousError = currentError;
            return (aP * aPIDproportion + aI * aPIDintegral + aD * aPIDderivative);
        }

        //---------------------PID CONTROLLER END--------------

        // Use this for initialization
        private void Start()
        {
            rb = GetComponent<Rigidbody>();
            safeposition = transform.position;
            safeRotation = transform.rotation;
            RaceFinished = false;
            colliding = false;
        }

        // Update is called once per frame
        private void Update()
        {
            //   if (manager.CanRace == true)
            {
                rb.AddRelativeTorque(Vector3.up * (rotationSpeed * Input.GetAxis("Horizontal2") * Time.deltaTime));
                //rb.maxAngularVelocity = 2;
                //lVelocity.z += Input.GetAxis ("Throttle") * speed * Time.deltaTime;

                rb.AddRelativeForce(0, 0, (Input.GetAxis("Break2") * Break) * Time.deltaTime, ForceMode.Impulse);
                // lVelocity.x += Input.GetAxis ("Strafe") * strafeSpeed * Time.deltaTime;
                // adds all force to object
                rb.AddRelativeForce(Input.GetAxis("Strafe2") * strafeSpeed * Time.deltaTime, 0, Input.GetAxis("Throttle2") * speed * Time.deltaTime, ForceMode.Force);// movement
            }

            //Debug.Log (rb.velocity.magnitude);
        }

        private void OnCollisionEnter(Collision collision)
        {
            colliding = true;
            //rb.velocity *= 0.5f;
            rb.velocity = Vector3.Scale(rb.velocity, new Vector3(0.1f, 0.1f, 0.1f));
        }

        private void OnCollisionStay(Collision collision)
        {
            colliding = true;
            rb.AddForce(collision.impulse);
        }

        private void OnCollisionExit(Collision collision)
        {
            colliding = false;
        }

        private void FixedUpdate()
        {
            float dt = Time.fixedDeltaTime;

            Ray ray = new Ray(transform.position, -transform.up);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Hover_Ray, 1 << 10))
            {
                onTrack = true;

                // torque required to rotate to specified rotation

                Vector3 axis = Vector3.Cross(transform.up.normalized, hit.normal.normalized);// this gets the axis of rotation
                float torqueAngle = Mathf.Asin(axis.magnitude);// this calculates the angle with the magnitude of the earlier cross product
                Vector3 deltaRotation = axis.normalized * torqueAngle / Time.fixedDeltaTime;// rotation over time

                // Rotate to track normal using torque
                rb.AddTorque(transform.rotation * (Quaternion.Inverse(transform.rotation) * deltaRotation) * 0.09f, ForceMode.Impulse);

                // hover force using PID controller
                rb.AddRelativeForce(0, PIDoutput(Hover_Height - hit.distance, dt), 0, ForceMode.Force);

                //Debug.Log (hit.collider.ToString ());
                //  safeposition = transform.position;

                //safeRotation = Quaternion.LookRotation (hit.normal);

                if (colliding == false)
                {
                    //safeposition = transform.position;
                    safeposition = hit.point + new Vector3(0, Hover_Height, 0);
                    safeRotation = (Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation);
                }
            }
            else
            {
                // if the racer is too far away from the safeposition respawn
                if (Vector3.Distance(safeposition, transform.position) > 5 && onTrack == true)
                {
                    // this problem could be fixed with a cylinder collider at the front
                    transform.position = safeposition;
                    transform.rotation = safeRotation;

                    //rb.velocity = Vector3.zero;
                    Debug.Log("flying");
                }
            }
        }
    }
}