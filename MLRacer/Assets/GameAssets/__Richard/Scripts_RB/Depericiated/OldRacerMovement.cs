﻿using System;
using UnityEngine;
using UnityEngine.UI;

#if (UNITY_PS4)

using UnityEngine.PS4;

#endif

namespace Assets.GameAssets.__Richard.Scripts_RB.Depericiated
{
    public class OldRacerMovement : MonoBehaviour
    {
        //  [SerializeField]
        // RaceManager manager;// set in inspector

        public sColor color;
        public sString stickYaw;
        public sString stickRoll;
        public sString stickThrottle;
        public sInt ControllerId;

        public Transform racer;

        public sVector3 scriptOrientation;

        private Vector3 orientation;

        public sFloat maxRollAngle;
        public sFloat maxYawAngle;
        public sFloat maxPitchAngle;

        public sFloat Throttle;

        public sInt speed;
        public sInt strafe;
        public sInt yaw;

        //  [SerializeField] float speed=5;
        [SerializeField]
        private float Break = 5;

        [SerializeField]
        private float strafeSpeed = 5;

        [SerializeField]
        private float rotationSpeed = 1;

        [SerializeField]
        private float Hover_Force;

        [SerializeField]
        private float Hover_Ray;// set in inspector

        public Text countdown;

        public bool winState = false;
        private bool onTrack = false;

        private bool ResultsShown = false;
        private bool CanRace = false;
        private bool timeSet = true;

        [SerializeField]
        private float Hover_Height;// set in inspector

        [SerializeField]
        private float strafeDrag = 20;

        public string YawAxisName = "Horizontal";
        public string ThrottleAxisName = "Throttle";
        public string StrafeAxisname = "Strafe";
        public string BrakeAxisname = "Break";

        public bool RaceFinished = false;

        public double raceTime;

        public double startTime;

        private bool localgo;

        private bool colliding = false;
        private Vector3 safeposition; //=new Vector3 (0,5,0);
        private Quaternion safeRotation;
        private Vector3 lVelocity;
        private Rigidbody rb;

        //---------------------PID CONTROLLER START--------------
        [SerializeField]
        private float PIDproportion = 1;//initial force

        [SerializeField]
        private float PIDintegral = 0; // time taken

        [SerializeField]
        private float PIDderivative = 0.1f;//damper

        private float previousError;
        private float P, I, D;

        private float PIDoutput(float currentError, float DeltaTime)
        {
            P = currentError;
            I += P * DeltaTime;
            D = (P - previousError) / DeltaTime;
            previousError = currentError;
            return (P * PIDproportion + I * PIDintegral + D * PIDderivative);
        }

        //---------------------PID CONTROLLER END--------------

        //---------------------PID angle CONTROLLER START--------------
        public float aPIDproportion = 1;//initial force

        public float aPIDintegral = 0;
        public float aPIDderivative = 0.1f;//damper
        private float apreviousError;
        private float aP, aI, aD;

        private float aPIDoutput(float currentError, float DeltaTime)
        {
            aP = currentError;
            aI += aP * DeltaTime;
            aD = (aP - apreviousError) / DeltaTime;
            apreviousError = currentError;
            return (aP * aPIDproportion + aI * aPIDintegral + aD * aPIDderivative);
        }

        //---------------------PID CONTROLLER END--------------

        // Use this for initialization
        private void Start()
        {
            rb = GetComponent<Rigidbody>();
            safeposition = transform.position;
            safeRotation = transform.rotation;
            RaceFinished = false;
            colliding = false;

#if (UNITY_PS4)
        Debug.Log(color.Value.r);
        PS4Input.PadSetLightBar( ControllerId.Value, (int)(color.Value.r*255),(int) (color.Value.g * 255), (int)(color.Value.b)*255);
#endif
        }

        // Update is called once per frame
        private void Update()
        {
            //Debug.Log (rb.velocity.magnitude);
        }

        private void OnCollisionEnter(Collision collision)
        {
            colliding = true;
            //rb.velocity *= 0.5f;
            rb.velocity = Vector3.Scale(rb.velocity, new Vector3(0.1f, 0.1f, 0.1f));
        }

        private void OnCollisionStay(Collision collision)
        {
            colliding = true;
            rb.AddForce(collision.impulse);
        }

        private void OnCollisionExit(Collision collision)
        {
            colliding = false;
        }

        private void FixedUpdate()
        {
            KeyCode codeOption = (KeyCode)Enum.Parse(typeof(KeyCode), "Joystick" + ((ControllerId.Value + 1) + "Button1").ToString(), true); // this is the O button
            //PS4Input.

#if (UNITY_PS4)
        {
            //  Debug.Log("PS4");
            if (Input.GetKey(codeOption))
                PS4Input.PadResetOrientation(ControllerId.Value);

            // the orientation values go from 0 to 1 to need to be multiplied by the max amount
            orientation.x = -PS4Input.GetLastOrientation(ControllerId.Value).x;//* maxPitchAngle.Value;// pitch
            orientation.y = -PS4Input.GetLastOrientation(ControllerId.Value).y * maxYawAngle.Value;// yaw
            orientation.z = -PS4Input.GetLastOrientation(ControllerId.Value).z * maxRollAngle.Value; // roll
        }
#endif

#if UNITY_STANDALONE_WIN
            // Debug.Log("Stand Alone Windows");

            orientation.y = Input.GetAxis(stickYaw.Value);// * maxYawAngle.Value;// yaw
            orientation.z = Input.GetAxis(stickRoll.Value);// * maxRollAngle.Value; // roll

#endif

            scriptOrientation.Value = orientation;

            racer.transform.localRotation = Quaternion.Euler(orientation.z, 90, 0);

            //  if (manager.CanRace == true)
            {
                rb.AddRelativeTorque(Vector3.up * (yaw.Value * orientation.y * Time.deltaTime));
                //rb.maxAngularVelocity = 2;
                //lVelocity.z += Input.GetAxis ("Throttle") * speed * Time.deltaTime;

                //rb.AddRelativeForce(0, 0, (Input.GetAxis(BrakeAxisname) * Break) * Time.deltaTime, ForceMode.Impulse);
                // lVelocity.x += Input.GetAxis ("Strafe") * strafeSpeed * Time.deltaTime;
                // adds all force to object
                rb.AddRelativeForce(orientation.z * strafe.Value * Time.deltaTime, 0, -Input.GetAxis(stickThrottle.Value) * speed.Value * Time.deltaTime, ForceMode.Force);// movement
            }

            float dt = Time.fixedDeltaTime;
            Ray ray = new Ray(transform.position, -transform.up);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Hover_Ray, 1 << 10))
            {
                onTrack = true;

                // torque required to rotate to specified rotation

                Vector3 axis = Vector3.Cross(transform.up.normalized, hit.normal.normalized);// this gets the axis of rotation
                float torqueAngle = Mathf.Asin(axis.magnitude);// this calculates the angle with the magnitude of the earlier cross product
                Vector3 deltaRotation = axis.normalized * torqueAngle / Time.fixedDeltaTime;// rotation over time

                // Rotate to track normal using torque
                rb.AddTorque(transform.rotation * (Quaternion.Inverse(transform.rotation) * deltaRotation) * 0.09f, ForceMode.Impulse);

                // hover force using PID controller
                rb.AddRelativeForce(0, PIDoutput(Hover_Height - hit.distance, dt), 0, ForceMode.Force);

                //Debug.Log (hit.collider.ToString ());
                //  safeposition = transform.position;

                //safeRotation = Quaternion.LookRotation (hit.normal);

                if (colliding == false)
                {
                    //safeposition = transform.position;
                    safeposition = hit.point;// + new Vector3(0,Hover_Height,0);
                    safeRotation = (Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation);
                }
            }
            else
            {
                // if the racer is too far away from the safeposition respawn
                if (Vector3.Distance(safeposition, transform.position) > 5 && onTrack == true)
                {
                    // this problem could be fixed with a cylinder collider at the front
                    // transform.position = safeposition;
                    //transform.rotation = safeRotation;

                    rb.MovePosition(safeposition);

                    rb.MoveRotation(safeRotation);
                    //rb.velocity = Vector3.zero;
                    Debug.Log("flying");
                }
            }
        }
    }
}