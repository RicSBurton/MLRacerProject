﻿using UnityEngine;

[CreateAssetMenu]
public class PIDData : ScriptableObject
{
    [SerializeField]
    private float _P, _I, _D;

    public float P => _P;

    public float I => _I;

    public float D => _D;
}