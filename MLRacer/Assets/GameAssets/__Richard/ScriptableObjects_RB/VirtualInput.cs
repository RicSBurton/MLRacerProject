﻿using UnityEngine;

[CreateAssetMenu]
public class VirtualInput : ScriptableObject
{
    public int ControllerId
    {
        get => _controllerID;
        set => _controllerID = value;
    }

    private int _controllerID;

    [Space]
    [Header("Sticks ")]
    [SerializeField] private string _leftStickHorizontalName;

    [SerializeField] private string _leftStickVerticalName;
    [SerializeField] private Vector2 _leftStick;

    [Space]
    [SerializeField] private string _rightStickHorizontalName;

    [SerializeField] private string _rightStickVerticalName;
    [SerializeField] private Vector2 _rightStick;

    #region("Stick Properties")

    [field: SerializeField]
    public Vector2 LeftStick
    {
        get
        {
            _leftStick.x = Input.GetAxis(_leftStickHorizontalName);
            _leftStick.y = Input.GetAxis(_leftStickVerticalName);

            return _leftStick;
        }

        //  set { ; }
    }

    public Vector2 RightStick
    {
        get
        {
            _rightStick.x = Input.GetAxis(_rightStickHorizontalName);
            _rightStick.y = Input.GetAxis(_rightStickVerticalName);

            return _rightStick;
        }
        //  set => _rightStick = value;
    }

    #endregion()

    [Space]
    [Header("Triggers ")]
    [SerializeField] private string _leftTriggerName;

    [SerializeField] private float _leftTrigger;

    [Space]
    [SerializeField] private string _rightTriggerName;

    [SerializeField] private float _rightTrigger;
    #region("Trigger Properties ")

    public float LeftTrigger
    {
        get
        {
            return _leftTrigger = Input.GetAxis(_leftTriggerName);
        }
        // set => _leftTrigger = value;
    }

    public float RightTrigger
    {
        get
        {
            return _rightTrigger = Input.GetAxis(_rightTriggerName);
        }
        // set => _rightTrigger = value;
    }

    #endregion()

    [Space]
    [Header("Bumpers ")]
    [SerializeField] private string _leftBumperName;

    [SerializeField] private bool _leftBumper;

    [Space]
    [SerializeField] private string _rightBumperName;

    [SerializeField] private bool _rightBumper;
    #region(" Bumper Properties ")

    public bool LeftBumper
    {
        get => _leftBumper;
        set => _leftBumper = value;
    }

    public bool RightBumper
    {
        get => _rightBumper;
        set => _rightBumper = value;
    }

    #endregion()

    [Header("Face Buttons ")]
    [SerializeField] private string _buttonUpName;

    [SerializeField] private string _buttonDownName;
    [SerializeField] private string _buttonLeftName;
    [SerializeField] private string _buttonRightName;

    [Space]
    [SerializeField] private bool _buttonUp;

    [SerializeField] private bool _buttonDown;
    [SerializeField] private bool _buttonLeft;
    [SerializeField] private bool _buttonRight;
    #region("Face Button  Properties ")

    public bool ButtonUp
    {
        get => _buttonUp;
        set => _buttonUp = value;
    }

    public bool ButtonDown
    {
        get => _buttonDown;
        set => _buttonDown = value;
    }

    public bool ButtonLeft
    {
        get => _buttonLeft;
        set => _buttonLeft = value;
    }

    public bool ButtonRight
    {
        get => _buttonRight;
        set => _buttonRight = value;
    }

    #endregion()

    [Space]
    [Header("D-Pad ")]
    [SerializeField] private string _dPadVerticalName;

    [SerializeField] private string _dPadHorizontalName;

    [Space]
    [SerializeField] private bool _dPadUP;

    [SerializeField] private bool _dPadDown;
    [SerializeField] private bool _dPadLeft;
    [SerializeField] private bool _dPadRight;
    #region(" Dpad Properties ")

    public bool DPadUp
    {
        get => _dPadUP;
        set => _dPadUP = value;
    }

    public bool DPadDown
    {
        get => _dPadDown;
        set => _dPadDown = value;
    }

    public bool DPadLeft
    {
        get => _dPadLeft;
        set => _dPadLeft = value;
    }

    public bool DPadRight
    {
        get => _dPadRight;
        set => _dPadRight = value;
    }

    #endregion()
}