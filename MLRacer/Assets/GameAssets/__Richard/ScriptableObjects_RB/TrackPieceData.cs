﻿using UnityEngine;

[CreateAssetMenu]
public class TrackPieceData : ScriptableObject
{
    [SerializeField] private bool canBeWrittenTo = false;
    [SerializeField] private GameObject _trackPiecePrefab;

    [SerializeField]
    private Vector3 _directionVector;

    [SerializeField]
    private Vector3 _endPointPosition;

    [SerializeField]
    private Quaternion _directionQuaternion;

    public string _name;

    public GameObject TrackPiecePrefab
    {
        get { return _trackPiecePrefab; }
    }

    public Vector3 EndPointPosition => _endPointPosition;

    public Quaternion DirectionQuaternion => _directionQuaternion;

    public void SetTrackPieceData(Vector3 endPointPosition)
    {
        if (!canBeWrittenTo)
        {
            return;
        }
        _endPointPosition = endPointPosition;
    }

    [ContextMenu("rename")]
    private void Rename()
    {
        name = _name;
    }

    private void OnValidate()
    {
        if (!canBeWrittenTo)
        {
            return;
        }
        _directionQuaternion = Quaternion.Euler(-_directionVector.x, -_directionVector.y, _directionVector.z);
    }
}